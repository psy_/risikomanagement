\babel@toc {german}{}
\beamer@sectionintoc {1}{Einleitende Worte}{3}{0}{1}
\beamer@sectionintoc {2}{Anforderungen an eine Risikomanagement-Software}{4}{0}{2}
\beamer@sectionintoc {3}{Werkzeuge}{5}{0}{3}
\beamer@subsectionintoc {3}{1}{Verinice}{6}{0}{3}
\beamer@subsectionintoc {3}{2}{eramba}{13}{0}{3}
\beamer@subsectionintoc {3}{3}{OpenNMS}{18}{0}{3}
\beamer@subsectionintoc {3}{4}{Imixs}{21}{0}{3}
\beamer@subsectionintoc {3}{5}{OpenVAS}{22}{0}{3}
\beamer@subsectionintoc {3}{6}{Sidoc}{26}{0}{3}
\beamer@sectionintoc {4}{Fazit}{33}{0}{4}
